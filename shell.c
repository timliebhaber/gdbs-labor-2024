#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LINE_LENGTH 1024
#define MAX_ARGS 64

void parse_command(char* line, char** args) {
    while (*line != '\0') {
        while (*line == ' ' || *line == '\t' || *line == '\n') {
            *line++ = '\0';
        }
        *args++ = line;
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') {
            line++;
        }
    }
    *args = '\0';
}

int main() {
    char line[MAX_LINE_LENGTH];
    char* args[MAX_ARGS];
    pid_t pid;
    int status;

    while (1) {
        printf("myshell> ");
        if (fgets(line, sizeof(line), stdin) == NULL) {
            perror("fgets failed");
            exit(EXIT_FAILURE);
        }

        // Remove newline character if present
        size_t len = strlen(line);
        if (len > 0 && line[len-1] == '\n') {
            line[len-1] = '\0';
        }

        parse_command(line, args);

        if (args[0] == NULL) {
            continue;  // Empty command
        }

        if (strcmp(args[0], "exit") == 0) {
            break;  // Exit the shell
        }

        pid = fork();

        if (pid < 0) {
            perror("fork failed");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            // Child process
            if (execv(args[0], args) == -1) {
                perror("execv failed");
                exit(EXIT_FAILURE);  // Exit child process if execv fails
            }
        } else {
            // Parent process
            do {
                pid_t wpid = waitpid(pid, &status, WUNTRACED);
                if (wpid == -1) {
                    perror("waitpid failed");
                    exit(EXIT_FAILURE);
                }
            } while (!WIFEXITED(status) && !WIFSIGNALED(status));
        }
    }

    return 0;
}